import copy
from collections import ChainMap

import pytest


RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
NETWORK_SECURITY_GROUP_NAME = "my-security-group"
RESOURCE_PARAMETERS = {
    "location": "eastus",
    "security_rules": [
        {
            "name": "rule1",
            "access": "Allow",
            "destination_address_prefix": "*",
            "destination_port_range": "80",
            "direction": "Inbound",
            "priority": 130,
            "protocol": "*",
            "source_address_prefix": "*",
            "source_port_range": "*",
        }
    ],
}
RAW_RESOURCE_PARAMETERS = {
    "location": "eastus",
    "properties": {
        "securityRules": [
            {
                "name": "rule1",
                "properties": {
                    "access": "Allow",
                    "destinationAddressPrefix": "*",
                    "destinationPortRange": "80",
                    "direction": "Inbound",
                    "priority": 130,
                    "protocol": "*",
                    "sourceAddressPrefix": "*",
                    "sourcePortRange": "*",
                },
            }
        ]
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of network security groups. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.network.network_security_groups.present = (
        hub.states.azure.network.network_security_groups.present
    )
    mock_hub.tool.azure.network.network_security_groups.convert_raw_network_security_groups_to_present = (
        hub.tool.azure.network.network_security_groups.convert_raw_network_security_groups_to_present
    )
    mock_hub.tool.azure.network.network_security_groups.convert_present_to_raw_network_security_groups = (
        hub.tool.azure.network.network_security_groups.convert_present_to_raw_network_security_groups
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **RAW_RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.network.network_security_groups",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert json == RAW_RESOURCE_PARAMETERS
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.network_security_groups.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Would create azure.network.network_security_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=RESOURCE_NAME,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.network_security_groups.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Created azure.network.network_security_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of network security groups. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.network.network_security_groups.present = (
        hub.states.azure.network.network_security_groups.present
    )
    mock_hub.tool.azure.network.network_security_groups.convert_raw_network_security_groups_to_present = (
        hub.tool.azure.network.network_security_groups.convert_raw_network_security_groups_to_present
    )
    mock_hub.tool.azure.network.network_security_groups.convert_present_to_raw_network_security_groups = (
        hub.tool.azure.network.network_security_groups.convert_present_to_raw_network_security_groups
    )
    mock_hub.tool.azure.network.network_security_groups.update_network_security_groups_payload = (
        hub.tool.azure.network.network_security_groups.update_network_security_groups_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    resource_parameters_update = {
        "location": "eastus",
        "security_rules": [
            {
                "name": "rule1",
                "access": "Allow",
                "destination_address_prefix": "*",
                "destination_port_range": "90",
                "direction": "Inbound",
                "priority": 135,
                "protocol": "*",
                "source_address_prefix": "*",
                "source_port_range": "*",
            }
        ],
        "tags": {"tag-new-key": "tag-new-value"},
    }

    resource_parameters_update_raw = {
        "location": "eastus",
        "properties": {
            "securityRules": [
                {
                    "name": "rule1",
                    "properties": {
                        "access": "Allow",
                        "destinationAddressPrefix": "*",
                        "destinationPortRange": "90",
                        "direction": "Inbound",
                        "priority": 135,
                        "protocol": "*",
                        "sourceAddressPrefix": "*",
                        "sourcePortRange": "*",
                    },
                }
            ]
        },
        "tags": {"tag-new-key": "tag-new-value"},
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **RAW_RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        assert resource_parameters_update_raw["location"] == json.get("location")
        assert resource_parameters_update_raw["tags"] == json.get("tags")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.network_security_groups.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Would update azure.network.network_security_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
        idem_resource_name=RESOURCE_NAME,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.network_security_groups.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NETWORK_SECURITY_GROUP_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Updated azure.network.network_security_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of network security groups. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.network.network_security_groups.absent = (
        hub.states.azure.network.network_security_groups.absent
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.network.network_security_groups.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NETWORK_SECURITY_GROUP_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.network.network_security_groups '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of network security groups. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.network.network_security_groups.absent = (
        hub.states.azure.network.network_security_groups.absent
    )
    mock_hub.tool.azure.network.network_security_groups.convert_raw_network_security_groups_to_present = (
        hub.tool.azure.network.network_security_groups.convert_raw_network_security_groups_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **RAW_RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NETWORK_SECURITY_GROUP_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.network_security_groups.absent(
        test_ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NETWORK_SECURITY_GROUP_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.network.network_security_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
        idem_resource_name=RESOURCE_NAME,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.network.network_security_groups.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NETWORK_SECURITY_GROUP_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.network.network_security_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of network security groups.
    """
    mock_hub.states.azure.network.network_security_groups.describe = (
        hub.states.azure.network.network_security_groups.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.network.network_security_groups.convert_raw_network_security_groups_to_present = (
        hub.tool.azure.network.network_security_groups.convert_raw_network_security_groups_to_present
    )
    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Network/networkSecurityGroups/{NETWORK_SECURITY_GROUP_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RAW_RESOURCE_PARAMETERS}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.network.network_security_groups.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.network.network_security_groups.present" in ret_value.keys()
    described_resource = ret_value.get("azure.network.network_security_groups.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=resource_id,
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state.get("tags") == old_state.get("tags")
        assert expected_old_state.get("security_rules") == old_state.get(
            "security_rules"
        )
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state.get("tags") == new_state.get("tags")
        assert expected_new_state.get("security_rules") == new_state.get(
            "security_rules"
        )
