URL = "https://docs.microsoft.com/en-us/rest/api/aks/"
DESC = "Azure Advisor is a personalized cloud consultant that helps you follow best practices to optimize your Azure deployments. For a more detailed overview, see the Azure Advisor product page."
