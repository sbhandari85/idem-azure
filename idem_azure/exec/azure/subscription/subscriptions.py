"""Exec module for managing Subscriptions."""
from collections import OrderedDict
from typing import Any
from typing import Dict

__func_alias__ = {"list_": "list"}


async def get(
    hub,
    ctx,
    name: str,
    resource_id: str,
) -> Dict[str, Any]:
    """Get subscription resource from resource_id.

    Args:
        name(str):
            The idem state run name
        resource_id(str):
            The resource_id of virtual machine

    Returns:
        Dict[str, Any]

    Examples:
        Calling this exec module function from the cli with resource_id:

        .. code-block:: bash

            idem exec azure.subscription.subscriptions.get name="value" resource_id="value"

        Using in a state:

        .. code-block:: yaml

            my_unmanaged_resource:
              exec.run:
                - path:  azure.subscription.subscriptions.get
                - kwargs:
                    name: "my_resource"
                    resource_id: "/providers/Microsoft.Subscription/aliases/{alias}"

    """
    result = dict(comment=[], result=True, ret=None)
    uri_parameters = OrderedDict({"aliases": "aliases"})

    response_get = await hub.exec.request.json.get(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2020-09-01",
        success_codes=[200],
    )
    if not response_get["result"]:
        result["comment"] = response_get["comment"]
        result["result"] = False
        result["ret"] = {"status": response_get["status"]}
        return result

    uri_parameter_values = hub.tool.azure.uri.get_parameter_value_in_dict(
        resource_id, uri_parameters
    )
    result[
        "ret"
    ] = hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present(
        resource=response_get["ret"],
        idem_resource_name=name,
        display_name=None,
        subscription_id=None,
        resource_id=resource_id,
        **uri_parameter_values,
    )

    return result


async def list_(hub, ctx) -> Dict:
    """List of subscriptions

    Returns:
        Dict[str, Any]

    Examples:
        Calling this exec module function from the cli with resource_id:

        .. code-block:: bash

            idem exec azure.subscription.subscriptions.list

        Using in a state:

        .. code-block:: yaml

            my_unmanaged_resource:
              exec.run:
                - path: azure.subscription.subscriptions.list


    """
    result = dict(comment=[], result=True, ret=[])
    uri_parameters = OrderedDict({"subscriptions": "subscription_id"})
    async for page_result in hub.tool.azure.request.paginate(
        ctx,
        url=f"{ctx['acct']['endpoint_url']}/subscriptions?api-version=2020-01-01",
        success_codes=[200],
    ):
        resource_list = page_result.get("value", None)
        if resource_list:
            for resource in resource_list:
                resource_id = resource["id"]
                uri_parameter_values = hub.tool.azure.uri.get_parameter_value_in_dict(
                    resource["id"], uri_parameters
                )
                result["ret"].append(
                    hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present(
                        resource=resource,
                        idem_resource_name=resource_id,
                        resource_id=resource_id,
                        alias=None,
                        display_name=None,
                        **uri_parameter_values,
                    )
                )

    return result
