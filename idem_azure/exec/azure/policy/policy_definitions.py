"""Exec module for managing Policy Definitions."""
from collections import OrderedDict
from typing import Any
from typing import Dict

__func_alias__ = {"list_": "list"}


async def get(
    hub,
    ctx,
    name: str,
    resource_id: str,
) -> Dict[str, Any]:
    """Get policy definition resource from resource_id.

    Args:
        name(str):
            The idem state run name
        resource_id(str):
            The resource_id of policy definition

    Returns:
        Dict[str, Any]

    Examples:
        Calling this exec module function from the cli with resource_id:

        .. code-block:: bash

            idem exec azure.policy.policy_definitions.get name="value" resource_id="value"

        Using in a state:

        .. code-block:: yaml

            my_unmanaged_resource:
              exec.run:
                - path:  azure.policy.policy_definitions.get
                - kwargs:
                    name: "my_resource"
                    resource_id: "/subscriptions/{subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{policy_definition_name}"

    """
    result = dict(comment=[], result=True, ret=None)
    uri_parameters = OrderedDict(
        {
            "policyDefinitions": "policy_definition_name",
            "subscriptions": "subscription_id",
        }
    )

    response_get = await hub.exec.request.json.get(
        ctx,
        url=f"{ctx.acct.endpoint_url}{resource_id}?api-version=2021-06-01",
        success_codes=[200],
    )
    if not response_get["result"]:
        result["comment"] = response_get["comment"]
        result["result"] = False
        result["ret"] = {"status": response_get["status"]}
        return result

    uri_parameter_values = hub.tool.azure.uri.get_parameter_value_in_dict(
        resource_id, uri_parameters
    )
    result[
        "ret"
    ] = hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present(
        resource=response_get["ret"],
        idem_resource_name=name,
        resource_id=resource_id,
        **uri_parameter_values,
    )

    return result


async def list_(hub, ctx) -> Dict:
    """List of policy definitions

    Returns:
        Dict[str, Any]

    Examples:
        Calling this exec module function from the cli with resource_id:

        .. code-block:: bash

            idem exec azure.policy.policy_definitions.list

        Using in a state:

        .. code-block:: yaml

            my_unmanaged_resource:
              exec.run:
                - path: azure.policy.policy_definitions.list


    """
    result = dict(comment=[], result=True, ret=[])
    subscription_id = ctx.acct.subscription_id
    uri_parameters = OrderedDict(
        {
            "policyDefinitions": "policy_definition_name",
            "subscriptions": "subscription_id",
        }
    )
    async for page_result in hub.tool.azure.request.paginate(
        ctx,
        url=f"{ctx.acct.endpoint_url}/subscriptions/{subscription_id}"
        f"/providers/Microsoft.Authorization/policyDefinitions?api-version=2021-06-01",
        success_codes=[200],
    ):
        resource_list = page_result.get("value")
        if resource_list:
            for resource in resource_list:
                resource_id = resource["id"]
                uri_parameter_values = hub.tool.azure.uri.get_parameter_value_in_dict(
                    resource["id"], uri_parameters
                )
                result["ret"].append(
                    hub.tool.azure.policy.policy_definition.convert_raw_policy_definition_to_present(
                        resource=resource,
                        idem_resource_name=resource_id,
                        resource_id=resource_id,
                        **uri_parameter_values,
                    )
                )
    return result
